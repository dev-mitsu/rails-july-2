# 文字列ではなく、数を入力させることもできる。

puts "数学の得点を入力してください。"
m_point = gets.to_i #.to_iで文字列を数に変換する。

puts "英語の得点を入力してください。"
e_point = gets.to_i

total = m_point + e_point

puts "合計点は#{total}点です。"
