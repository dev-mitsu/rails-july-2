a = 1
b = 2

#条件式の種類

puts "a == b : #{a == b}" #aとbが等しいとき、true

puts "a != b : #{a != b}" #aとbが等しくないとき、true

puts "a < b : #{a < b}" #aがbより小さい時、true

puts "a <= b : #{a <= b}" #aがb以下のとき、true

puts "a > b : #{a > b}" #aがbより大きいとき、true

puts "a >= b : #{a >= b}" #aがb以上のとき、true


# 演習：a,bの値を替えて試してみよう
